const tabsList = document.querySelector('.nav-list');
const tabsText = document.querySelectorAll('.nav__text-box div');
tabsList.addEventListener('click', (event)=> {
    const target = event.target;
    const index = target.getAttribute('data-li')
    
    target.closest('ul').querySelector('.active').classList.remove('active');
    target.classList.add('active');
    
    tabsText.forEach((item) => {
        const tabsId = item.getAttribute('data-li');
        if (index !== tabsId) {
            item.classList.add('nav__text-box-none');
        }else{
            item.classList.remove('nav__text-box-none');
        }
    });
} );



const arr = [`src="images/graphic-design/graphic-design5.jpg"  class="section-img_item all-img graphic-design" alt="Graphic Design"`,
`src="images/graphic-design/graphic-design6.jpg" class="section-img_item all-img graphic-design" alt="Graphic Design"`,`class="section-img_item all-img graphic-design" src="images/graphic-design/graphic-design3.jpg"  alt="Graphic Design"`,`class="section-img_item all-img web-design" src="images/web-design/web-design1.jpg"  alt="Web Wesign"`,`class="section-img_item all-img web-design" src="images/web-design/web-design2.jpg" alt="Web Design"`,`class="section-img_item all-img web-design" src="images/web-design/web-design3.jpg"  alt="Web Design"`,`class="section-img_item all-img landing-pages" src="images/landing-page/landing-page1.jpg" alt="Landing pages"`,`class="section-img_item all-img landing-pages" src="images/landing-page/landing-page2.jpg"  alt="Landing pages"`,`class="section-img_item all-img landing-pages" src="images/landing-page/landing-page3.jpg"  alt="Landing pages"`,`class="section-img_item all-img wordpress" src="images/wordpress/wordpress3.jpg"  alt="Wordpress"`,`class="section-img_item all-img wordpress" src="images/wordpress/wordpress4.jpg"  alt="Wordpress"`,`class="section-img_item all-img wordpress" src="images/wordpress/wordpress5.jpg"  alt="Wordpress"`];   

const list = document.querySelector(".section-box_img");
function loadImg () {
    document.querySelector(".section-box_img").insertAdjacentHTML('beforeend', arr.map(function(item){
    return `<img ${item}>`}).join(""));
    document.querySelector(".btn-load").classList.add("hidden");
};
document.querySelector(".btn-load").addEventListener("click", loadImg);

const showImg = function (event) {
    const currentItem = event.target.closest(".section__linst-item");
    const dataName = currentItem.dataset.name;
    const tabs = document.querySelectorAll(".section-box_img > img");
    description = document.querySelector(`img.${dataName}`);
    tabs.forEach((element) => {
        element.classList.add("hidden");
        if (element.classList.contains(dataName)) element.classList.remove("hidden");
    });
};
document.querySelector(".section__navigation").addEventListener("click", showImg);







// $(document).ready(function() {
//     $('.slider-person-info').slick({
//         asNavFor: ".people-slider",
//         slidesToShow: 1,
//         slidesToScroll: 1,
//         speed: 1500,
//         easing: 'linear',
//         arrows: false,
//         infinite: false
//     })
//     $('.people-slider').slick({
//         asNavFor: ".slider-person-info",
//         slidesToShow: 4,
//         slidesToScroll: 1,
//         speed: 1500,
//         easing: 'easeOutElastic',
//         arrows: true,
//         focusOnSelect: true,
//         infinite: false
//     });
// });

// $('.single-item').slick({
//     infinite: true,
//     dots: true,
//     slidesToShow: 3,
//     slidesToScroll: 1
//   });

$(document).ready(function() {
    $('.slider-person').slick({
        asNavFor: ".people-slider",
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 1500,
        easing: 'linear',
        arrows: false,
        infinite: false
    })
    $('.people-slider').slick({
        asNavFor: ".slider-person",
        slidesToShow: 4,
        slidesToScroll: 1,
        speed: 1500,
        easing: 'easeOutElastic',
        arrows: true,
        focusOnSelect: true,
        infinite: false
    });
});